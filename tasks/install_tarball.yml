---
# Copyright 2017 FUJITSU LIMITED

- name: Install OpenJDK
  package: name="{{ java_package_name }}" state=present

- name: Check if temporary download directory already exists
  stat: path="{{ download_tmp_dir }}"
  register: st

- name: Create download temporary directory
  file: path="{{ download_tmp_dir }}" state=directory force=true
  when: download_tmp_dir is defined and not st.stat.exists

- name: Fetch logstash tar.gz file
  get_url:
    dest="{{ download_tmp_dir }}/logstash-{{ logstash_version }}.tar.gz"
    url="{{ logstash_download }}/logstash-{{ logstash_version }}.tar.gz"
    timeout="{{ download_timeout }}"
  register: get_url_result
  until: get_url_result.state == 'file'
  retries: 5
  delay: 1

- name: Uncompress the logstash tar
  unarchive:
    src="{{ download_tmp_dir }}/logstash-{{ logstash_version }}.tar.gz"
    dest="{{ logstash_uncompress_dest }}"
    copy=no

- name: Set proper attributes of logstash files
  file:
    path="{{ logstash_uncompress_dest }}/logstash-{{ logstash_version }}"
    state=directory
    owner=root
    group="{{ log_transformer_group }}"
    recurse=yes

- name: Link logstash files
  file:
    src="{{ logstash_uncompress_dest }}/logstash-{{ logstash_version }}"
    dest="{{ logstash_dest }}"
    state=link
    owner=root
    group="{{ log_transformer_group }}"
    force=no

- name: Create a log directory
  file:
    path="{{ log_transformer_log_dir }}"
    state=directory
    owner="{{ log_transformer_user }}"
    group="{{ log_transformer_group }}"
    mode="u=rwX,g=rX,o="
    recurse=yes

- name: Create a directory for configuration files
  file:
    path="{{ log_transformer_conf_dir }}"
    state=directory
    owner=root
    group="{{ log_transformer_group }}"
    mode=0750
