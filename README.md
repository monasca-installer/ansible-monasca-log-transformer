# Variables explained

## Required Variables
* **zookeeper_hosts** - Example: 192.168.10.4:2181
* **kafka_hosts** - Example: 192.168.10.5:2181

## Optional

* **download_timeout** - defines a time for timeout when downloading ELK tar files, defaults to 600
* **download_tmp_dir** - location where logstash is being download (default /tmp)
* **tranformed_log_topic** - defines the Kafka topic id between transformer and persister, defaults to 'transformed-log'
* **log_topic** - defines the Kafka topic id between monasca-log-api and transformer, defaults to 'log'
* **logstash_version** - defines the logstash version to be installed, defaults to '2.2.0'
* **log_transformer_conf_dir** - a directory where logstash configuration files are stored, defaults to '/etc/monasca/log'
* **log_transformer_conf_file** - full path to the logstash configuration file, defaults to '/etc/monasca/log/transformer.conf'
* **log_transformer_consumer_threads** - how many threads to use to run transformer
* **log_transformer_fetch_msg_max_bytes** - number of bytes of messages to attempt to fetch for each topic-partition in each fetch request (default **1048576**)
* **log_transformer_service_name** - name of the system service, defaults to 'monasca-log-transformer'
* **log_transformer_install_user_group** - should user and group be installed in system, defaults ```True```

# Logstash Version

It is possible to control which Logstash will be installed by the mean of
**logstash_version** variable. However, bear in mind that embedded plugins
may have different configurations options, something could have been
deprecated for instance.

# Used plugins
**monasca-log-persister** uses Kafka and ElasticSearch as, respectively,
input and output plugins. In case of any doubts please refer to those
plugins documentation. Make sure that documentation must match **logstash_version**
that is specified.

# Process and resources control (user,group)

Following variables are used to narrow down **monasca-log-transformer** access only to those
resources which it actually needs.

* **log_transformer_user** - user name for **monasca-log-transformer** resources
* **log_transformer_group** - group name for **monasca-log-transformer** resources

## License

Apache License, Version 2.0

## Author Information

Tomasz Trebski
